package com.biom4st3r.dynocaps;

import com.biom4st3r.dynocaps.components.DynocapComponent;
import com.biom4st3r.dynocaps.components.IDynocapComponent;
import com.biom4st3r.dynocaps.guis.DynocapConfigGui;

import io.netty.buffer.Unpooled;
import net.fabricmc.fabric.api.network.ClientSidePacketRegistry;
import net.fabricmc.fabric.api.network.ServerSidePacketRegistry;
import net.minecraft.client.network.packet.CustomPayloadS2CPacket;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.server.network.packet.CustomPayloadC2SPacket;
import net.minecraft.util.Identifier;
import net.minecraft.util.PacketByteBuf;

/**
 * Packets
 */
public final class Packets {

    private static final Identifier 
        REQUEST_MAX_VALUES = new Identifier(ModInit.MODID, "reqmaxval"),
        REQUEST_SETTING_CHANGE = new Identifier(ModInit.MODID,"reqsetchg"),
        SEND_MAX_VALUES = new Identifier(ModInit.MODID, "sndmaxval")
        ;

    public static class SERVER
    {
        public static void init()
        {
            ServerSidePacketRegistry.INSTANCE.register(REQUEST_MAX_VALUES, (context,pbb)->
            {
                context.getTaskQueue().execute(()->
                {
                    ((ServerPlayerEntity)context.getPlayer()).networkHandler.sendPacket(sendMaxValues(ModInit.config.maxWidth, ModInit.config.maxHeight, ModInit.config.maxDepth));
                });
            });
            ServerSidePacketRegistry.INSTANCE.register(REQUEST_SETTING_CHANGE, (context,pbb)->
            {
                int width = pbb.readInt();
                int height = pbb.readInt();
                int depth = pbb.readInt();
                int color = pbb.readInt();
                boolean placeAir = pbb.readBoolean();
                String name = pbb.readString(25);
                PlayerEntity player = context.getPlayer();
                context.getTaskQueue().execute(()->
                {
                    ItemStack stack = player.getMainHandStack();
                    DynocapComponent component = null;
                    if(stack.getItem() == ModInit.DynoCap)
                    {
                        component = (DynocapComponent) ModInit.dynocapType.get(stack);
                    }
                    if(component == null ) return;
                    component.setWidth(Math.min(width, ModInit.config.maxWidth));
                    component.setHeight(Math.min(height, ModInit.config.maxHeight));
                    component.setDepth(Math.min(depth, ModInit.config.maxDepth));
                    component.setColor(color);
                    component.setPlaceAir(placeAir);
                    component.setName(name);
                });
            });
        }
        public static CustomPayloadS2CPacket sendMaxValues(int width,int height,int depth)
        {
            PacketByteBuf pbb = new PacketByteBuf(Unpooled.buffer());
            pbb.writeInt(width);
            pbb.writeInt(height);
            pbb.writeInt(depth);
            return new CustomPayloadS2CPacket(SEND_MAX_VALUES,pbb);
        }

    }
    public static class CLIENT
    {
        public static void init()
        {
            ClientSidePacketRegistry.INSTANCE.register(SEND_MAX_VALUES, (context,pbb)->
            {
                int maxWidth = pbb.readInt();
                int maxHeight = pbb.readInt();
                int maxDepth = pbb.readInt();
                context.getTaskQueue().execute(()->
                {
                    DynocapConfigGui.maxWidth = maxWidth;
                    DynocapConfigGui.maxHeight = maxHeight;
                    DynocapConfigGui.maxDepth = maxDepth;
                });
            });
        }
        public static CustomPayloadC2SPacket requestMaxValues()
        {
            PacketByteBuf pbb = new PacketByteBuf(Unpooled.buffer(0));
            return new CustomPayloadC2SPacket(REQUEST_MAX_VALUES, pbb);
        }
        public static CustomPayloadC2SPacket requestSettingsChange(IDynocapComponent component)
        {
            PacketByteBuf pbb = new PacketByteBuf(Unpooled.buffer());
            pbb.writeInt(component.getWidth());
            pbb.writeInt(component.getHeight());
            pbb.writeInt(component.getDepth());
            pbb.writeInt(component.getColor());
            pbb.writeBoolean(component.getPlaceAir());
            pbb.writeString(component.getName(),25);
            return new CustomPayloadC2SPacket(REQUEST_SETTING_CHANGE, pbb);
        }
    }

    
}