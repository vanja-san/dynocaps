package com.biom4st3r.dynocaps;

import com.mojang.datafixers.Dynamic;

import blue.endless.jankson.annotation.Nullable;
import net.minecraft.block.BlockState;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.datafixer.NbtOps;
import net.minecraft.fluid.FluidState;
import net.minecraft.fluid.Fluids;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.Tag;
import net.minecraft.util.Identifier;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3i;
import net.minecraft.util.registry.Registry;
import net.minecraft.world.World;

/**
 * BlockStorage is a conveniant way to store and serialize all data relevant to a block<p>
 * <b>relativePos</b> - position of the block in relation to the cube<p>
 * <b>state</b> - the BlockState of the block<p>
 * <b>entityTag</b> - the tag of the BlockEntity. <b>null</b> if it wasn't a blockEntity
 */
public class BlockStorage {
    public Vec3i relativePos;
    public BlockState state;
    @Nullable
    public CompoundTag entityTag;
    @LazyLoaded
    protected BlockEntityType<?> entityType = null;

    private BlockStorage(Vec3i pos, BlockState state, CompoundTag tag) {
        this.relativePos = pos;
        this.state = state;
        this.entityTag = tag;
    }

    public BlockStorage() {
    }
    /**
     * Checks if entityTag is null
     * @return
     */
    public boolean isBlockEntity()
    {
        return entityTag != null;
    }

    public @Nullable BlockEntityType<?> getEntityType()
    {
        if(entityTag == null) return null;
        if(entityType == null)
        {
            entityType = Registry.BLOCK_ENTITY_TYPE.get(new Identifier(entityTag.getString("id")));
        }
        return entityType;
    }

    public FluidState getFluidState()
    {
        return state.getFluidState();
    }

    public boolean isFluid()
    {
        return state.getFluidState() != Fluids.EMPTY.getDefaultState();
    }
    
    public @Nullable BlockEntity getBlockEntity()
    {
        if(!isBlockEntity()) return null;
        else
        {
            return BlockEntity.createFromTag(entityTag);
        }
    }

    public static BlockStorage newBlockStorage(World world, BlockPos pos, Vec3i vpos) {
        BlockStorage storage = new BlockStorage(vpos, world.getBlockState(pos), null);
        BlockEntity entity;
        if ((entity = world.getBlockEntity(pos)) != null) {
            storage.entityTag = entity.toTag(new CompoundTag());
        }
        return storage;
    }

    public static final String 
        POS = "a", 
        STATE = "b", 
        TYPE = "c", 
        ENTITY_TAG = "d";

    public Tag serializeBlockState(BlockState state) {
        Dynamic<Tag> serialized = BlockState.serialize(NbtOps.INSTANCE, state);
        return serialized.getValue();
    }

    public BlockState deserializeBlockState(Tag tag) {
        return BlockState.deserialize(new Dynamic<>(NbtOps.INSTANCE, tag));
    }

    public CompoundTag toTag(CompoundTag tag) {
        tag.putLong(POS, BlockPos.asLong(relativePos.getX(), relativePos.getY(), relativePos.getZ()));
        tag.put(STATE, serializeBlockState(state));
        tag.put(ENTITY_TAG, entityTag != null ? entityTag : new CompoundTag());
        return tag;
    }

    public BlockStorage fromTag(CompoundTag tag) {
        BlockPos bpos = BlockPos.fromLong(tag.getLong(POS));
        relativePos = new Vec3i(bpos.getX(), bpos.getY(), bpos.getZ());
        state = deserializeBlockState(tag.get(STATE));
        entityTag = tag.getCompound(ENTITY_TAG);
        if(entityTag.getSize() == 0) entityTag = null;
        return this;
    }
}