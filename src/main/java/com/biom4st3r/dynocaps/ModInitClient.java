package com.biom4st3r.dynocaps;

import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.function.Supplier;

import com.biom4st3r.biow0rks.ModelInjectionRegistry;
import com.biom4st3r.dynocaps.components.IDynocapComponent;
import com.biom4st3r.dynocaps.guis.CapCaseController;
import com.biom4st3r.dynocaps.guis.CapCaseGui;

import it.unimi.dsi.fastutil.ints.Int2ObjectMap;
import net.fabricmc.api.ClientModInitializer;
import net.fabricmc.fabric.api.client.rendering.v1.ColorProviderRegistry;
import net.fabricmc.fabric.api.client.screen.ScreenProviderRegistry;
import net.fabricmc.fabric.api.renderer.v1.model.FabricBakedModel;
import net.fabricmc.fabric.api.renderer.v1.render.RenderContext;
import net.minecraft.block.BlockState;
import net.minecraft.client.render.model.BakedModel;
import net.minecraft.client.render.model.BakedQuad;
import net.minecraft.client.render.model.json.ModelItemPropertyOverrideList;
import net.minecraft.client.render.model.json.ModelTransformation;
import net.minecraft.client.texture.Sprite;
import net.minecraft.inventory.Inventory;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.world.BlockRenderView;

/**
 * ModInitClient
 */
public class ModInitClient implements ClientModInitializer {

	// public static Field itemModelsField =
	// ReflectionHelper._getPrivateMethod(ItemRenderer.class, 3, true);
	// public static Field modelsField = ItemModels.class.getDeclaredFields()[1];
	// public static Field fieldModifiers = Field.class.getDeclaredFields()[4];
	public static Int2ObjectMap<BakedModel> modelList;
	public static BakedModel capCase;

	static {
		// itemModelsField.setAccessible(true);
		// modelsField.setAccessible(true);
		// fieldModifiers.setAccessible(true);
	}

	// public static void replaceModel(Item i,BakedModel model)
	// {
	// if(modelList == null){
	// try
	// {
	// fieldModifiers.setInt(modelsField, modelsField.getModifiers() &
	// ~Modifier.FINAL);

	// ItemModels models = (ItemModels)
	// itemModelsField.get(MinecraftClient.getInstance().getItemRenderer());
	// modelList = (Int2ObjectMap<BakedModel>) modelsField.get(models);
	// capCase = modelList.get(Item.getRawId(ModInit.CapCase));
	// }
	// catch(Throwable t)
	// {
	// t.printStackTrace();
	// }
	// }
	// modelList.put(Item.getRawId(i), model);
	// }

	@Override
	public void onInitializeClient() {

		Packets.CLIENT.init();
		// ItemColorProvider
		ColorProviderRegistry.ITEM.register((stack, layer) -> {
			if (layer == 0)
				return 0xFFFFFFFF;
			Optional<IDynocapComponent> Ocomponent;
			if ((Ocomponent = ModInit.dynocapType.maybeGet(stack)).isPresent()) {
				IDynocapComponent component = Ocomponent.get();
				return component.getColor();
			}
			return 0xFF00FF;
		}, ModInit.DynoCap);

		ColorProviderRegistry.ITEM.register((stack, layer) -> {
			if (layer == 0)
				return 0xFFFFFFFF;
			Inventory inv = ModInit.inventoryType.get(stack);
			if (inv.getInvStack(layer - 1).isEmpty())
				return 0xFFFFFFFF;
			return ModInit.dynocapType.get(inv.getInvStack(layer - 1)).getColor();
		}, ModInit.CapCase);
		ScreenProviderRegistry.INSTANCE.registerFactory(ModInit.containerid, (syncId, identifier, player, buf) -> {
			return new CapCaseGui(new CapCaseController(syncId, player.inventory), player);
		});
		// ModelLoadingRegistry.INSTANCE.registerAppender((manager,idConsumer)->
		// {
		// idConsumer.accept(new ModelIdentifier(new Identifier(ModInit.MODID,
		// "items/cap_case"), "inventory"));
		// });
		// ModInitClient.replaceModel(ModInit.CapCase, new CapCaseModel());
		// ModelInjectionRegistry.replaceBlockModel(Blocks.GRAVEL.getDefaultState(), new FabricGravelModel());

	}

	public static class FabricGravelModel implements FabricBakedModel, BakedModel {

		@Override
		public List<BakedQuad> getQuads(BlockState state, Direction face, Random random) {
			return ModelInjectionRegistry.getOriginalModel(state).getQuads(state, face, random);
		}

		@Override
		public boolean useAmbientOcclusion() {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public boolean hasDepth() {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public boolean isSideLit() {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public boolean isBuiltin() {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public Sprite getSprite() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public ModelTransformation getTransformation() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public ModelItemPropertyOverrideList getItemPropertyOverrides() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public boolean isVanillaAdapter() {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public void emitBlockQuads(BlockRenderView blockView, BlockState state, BlockPos pos,
				Supplier<Random> randomSupplier, RenderContext context) {
					// System.out.println("hello");
					context.fallbackConsumer().accept(ModelInjectionRegistry.getOriginalModel(state));
		}

		@Override
		public void emitItemQuads(ItemStack stack, Supplier<Random> randomSupplier, RenderContext context) {
			// TODO Auto-generated method stub

		}

	}

    
}