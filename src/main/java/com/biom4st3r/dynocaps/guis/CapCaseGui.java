package com.biom4st3r.dynocaps.guis;

import io.github.cottonmc.cotton.gui.client.CottonInventoryScreen;
import net.minecraft.entity.player.PlayerEntity;

/**
 * CapCaseGui
 */
public class CapCaseGui extends CottonInventoryScreen<CapCaseController> {

    public CapCaseGui(CapCaseController container, PlayerEntity player) {
        super(container, player);
    }

    @Override
    public void onClose() {
        super.onClose();
    }
    
}