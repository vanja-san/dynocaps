package com.biom4st3r.dynocaps.guis;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import com.biom4st3r.biow0rks.helpers.client.ClientHelper;
import com.biom4st3r.biow0rks.helpers.client.RenderHelper;
import com.biom4st3r.dynocaps.BlockStorage;
import com.biom4st3r.dynocaps.LazyLoaded;
import com.biom4st3r.dynocaps.ModInit;
import com.biom4st3r.dynocaps.components.IDynocapComponent;
import com.biom4st3r.dynocaps.mixin.BlockEntityAccessor;
import com.biom4st3r.dynocaps.mixin.QuadInvoker;
import com.google.common.base.Stopwatch;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.mojang.blaze3d.systems.RenderSystem;

import io.github.cottonmc.cotton.gui.GuiDescription;
import nerdhub.cardinal.components.api.component.ComponentProvider;
import net.fabricmc.fabric.api.client.render.fluid.v1.FluidRenderHandler;
import net.fabricmc.fabric.api.client.render.fluid.v1.FluidRenderHandlerRegistry;
import net.fabricmc.fabric.api.renderer.v1.model.FabricBakedModel;
import net.minecraft.block.BedBlock;
import net.minecraft.block.BlockRenderType;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.block.enums.BedPart;
import net.minecraft.client.render.OverlayTexture;
import net.minecraft.client.render.RenderLayer;
import net.minecraft.client.render.RenderLayers;
import net.minecraft.client.render.VertexConsumer;
import net.minecraft.client.render.VertexConsumerProvider;
import net.minecraft.client.render.block.entity.BlockEntityRenderDispatcher;
import net.minecraft.client.render.block.entity.BlockEntityRenderer;
import net.minecraft.client.render.model.BakedModel;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.client.util.math.Vector3f;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.fluid.FluidState;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.util.math.Quaternion;
import net.minecraft.util.math.Vec3i;
import net.minecraft.util.profiler.Profiler;

/**
 * DynocapFilledScreen
 */
public class DynocapFilledScreen extends NoPauseCottonScreen {

    public DynocapFilledScreen(GuiDescription description) {
        super(description);
        ItemStack stack = ClientHelper.player.get().getMainHandStack();
        this.component = ComponentProvider.fromItemStack(stack).getComponent(ModInit.dynocapType);
        this.blocks = component.getBoxContents(null, null, false);
    }


    public IDynocapComponent component;
    
    public List<BlockStorage> blocks;

    @LazyLoaded
    public HashMap<BlockState,BakedModel> stateToModelCache = Maps.newHashMap();
    @LazyLoaded
    public Map<BlockEntityType<?>,BlockEntity> BETypeToBECache = Maps.newHashMap();
    @LazyLoaded
    public Set<Vec3i> coords = Sets.newHashSet();
    public boolean firstPass = true;

    public static Profiler profiler = ClientHelper.client.getProfiler();
    
    @Override
    protected void init() {
        super.init();
    }

    @Override
    public void render(int mouseX, int mouseY, float delta) {
        super.render(mouseX, mouseY, delta);
        profiler.swap("dynocap_gui");
        renderBlock(this.getXMid()+55,this.getYMid()-5,delta);
    }

    public static Quaternion X_ROTATION = new Quaternion(Vector3f.POSITIVE_X, 180+25, true);
    /**
     * Local stack for {@link #DynocapFilledScreen}
     */
    public static MatrixStack stack = new MatrixStack();
    public void renderBlock(float x,float y, float delta)
    {
        RenderSystem.pushMatrix();
        // int minComponentDim = Math.min(component.getDepth(), Math.min(component.getHeight(), component.getWidth()));
        int maxComponentDim = Math.max(component.getDepth(), Math.max(component.getHeight(), component.getWidth()));
        int componentVolume = component.getDepth()*component.getWidth()*component.getHeight();
        float scale = Math.min(12, (maxComponentDim)*(200f/(componentVolume/4))); // quick and dirty Volume -> scale
        // float scale = 1f/(componentVolume)*800*maxComponentDim;
        // float scale = Math.max((1f/3)*(maxComponentDim), 12);
        RenderSystem.translatef(x, y,500); // Honestly just copied from PlayerInventoryScreen
        RenderSystem.scalef(1, 1, -1f); //    👆
        RenderSystem.color4f(1, 1, 1, 1);

        // Settings up my stack
        stack.push();
        stack.translate(component.getWidth()/2f, component.getHeight()/2f, component.getDepth()/2f); // translates to where the center of the model will be so that rotation is from the middle
        stack.scale(scale, scale, scale);
        stack.multiply(X_ROTATION);
        stack.multiply(new Quaternion(Vector3f.POSITIVE_Y,(float)((System.currentTimeMillis()/25.0D)%359.0D),true));
        stack.translate(-component.getWidth()/2f, -component.getHeight()/2f, -component.getDepth()/2f); // rotation is done so reset time

        VertexConsumerProvider.Immediate immediate = ClientHelper.client.getBufferBuilders().getEntityVertexConsumers(); // from PlayerInventoryScreen and lasttime i used getBlockBufferBuilders it was always null
        // BlockRenderContext context = new BlockRenderContext();
        PlayerEntity player = ClientHelper.player.get();
        float[] color = RenderHelper.intToRpg_F(player.world.getBiome(player.getBlockPos()).getFoliageColor());
        
        // Iterator takes 2-4ms
        // stream takes 2-4ms after bootstrap ¯\_(ツ)_/¯
        Random random = new Random();
        // Method microsec
        // stream  < 630
        // iterat  < 590 6.3% faster
        // iteratNoCull  < 1920 320% slower

        // Stopwatch sw = Stopwatch.createStarted();
        // streamBlocks(immediate, color, random, player, delta);
        iterateBlocks(immediate, color, random, player, delta);
        firstPass = false;

        immediate.draw();
        stack.pop();
        RenderSystem.popMatrix();
        // time+=sw.stop().elapsed(TimeUnit.MICROSECONDS);
        // count++;
        // if(count%20 == 0) System.out.println(time/(float)count);
    }
    // public int count = 0;
    // public Long time = 0L;

    public void iterateBlocks(VertexConsumerProvider.Immediate immediate,float[] color,Random random,PlayerEntity player,float delta)
    {
        Iterator<BlockStorage> storage = blocks.iterator();
        while(storage.hasNext())
        {
            BlockStorage stored = storage.next();
            BlockState iterState = stored.state;
            if(shouldIgnore(iterState)) continue;
            BakedModel iterModel;

            RenderLayer layer = RenderLayers.getBlockLayer(iterState);
            VertexConsumer vc = immediate.getBuffer(layer);
            int light = 0x00F000B0;
            if(firstPass)
            {
                if(iterState.isOpaque() && iterState.getBlock() != Blocks.SNOW)
                {
                    coords.add(stored.relativePos);
                }
            }
            if((iterModel = stateToModelCache.get(iterState)) == null)
            {
                stateToModelCache.put(iterState, RenderHelper.getModel(iterState));
                iterModel = stateToModelCache.get(iterState);
            }
            stack.push(); // Individual block placement
            stack.translate(stored.relativePos.getX(),stored.relativePos.getY() ,stored.relativePos.getZ());
            if(stored.isFluid())
            { // no culling support
                vc = immediate.getBuffer(RenderLayer.getTranslucent());
                FluidState fstate = stored.getFluidState();
                FluidRenderHandler handler = FluidRenderHandlerRegistry.INSTANCE.get(fstate.getFluid());
                RenderHelper.renderBox(0, 0, 0, 1, 1, (fstate.getLevel()/8f)-0.1f, 1, 
                    handler.getFluidSprites(null, null, iterState.getFluidState())[0], 
                    vc, 
                    stack, 
                    light, 
                    handler.getFluidColor(ClientHelper.world.get(),
                        ClientHelper.player.get().getBlockPos(), 
                        fstate));
            }
            if(iterState.getBlock().getRenderType(iterState) != BlockRenderType.INVISIBLE)
            {
                if(((FabricBakedModel)iterModel).isVanillaAdapter())
                {
                    for(Direction dir : Direction.values())
                    {
                        if(!coords.contains(stored.relativePos.offset(dir,1)))
                        {
                            QuadInvoker.renderQuad(stack.peek(), vc, color[0], color[1], color[2], iterModel.getQuads(null, dir, random), light, OverlayTexture.DEFAULT_UV);
                        }
                    }
                    QuadInvoker.renderQuad(stack.peek(), vc, color[0], color[1], color[2], iterModel.getQuads(null, null, random), light, OverlayTexture.DEFAULT_UV);
                }
                else
                {
                    ClientHelper.blockModelRenderer.render(player.world, iterModel, iterState, BlockPos.ORIGIN, stack, vc, false, random, 42L, OverlayTexture.DEFAULT_UV);
                }

            }
            BlockEntityType<?> type = null;
            if(stored.isBlockEntity()) 
            {
                type = stored.getEntityType();
                if(BETypeToBECache.get(type) == null) cacheBlockEntity(type, stored);
            }
            BlockEntityRenderer<BlockEntity> renderer = type == null ? null : BlockEntityRenderDispatcher.INSTANCE.get(BETypeToBECache.get(type));
            if(stored.isBlockEntity() && renderer != null && type != BlockEntityType.MOB_SPAWNER)
            {
                renderer.render(BETypeToBECache.get(type), delta, stack, immediate, light, OverlayTexture.DEFAULT_UV);
            }
            stack.pop();
        }
    }

    public void streamBlocks(VertexConsumerProvider.Immediate immediate,float[] color,Random random,PlayerEntity player,float delta)
    {
        blocks.stream().filter((bstorage)->!shouldIgnore(bstorage.state)).forEach((stored)->
        {

            BlockState iterState = stored.state;
            BakedModel iterModel;
            RenderLayer layer = RenderLayers.getBlockLayer(iterState);
            VertexConsumer vc = immediate.getBuffer(layer);
            if(firstPass)
            {
                if(iterState.isOpaque() && iterState.getBlock() != Blocks.SNOW)
                {
                    coords.add(stored.relativePos);
                }
            }
            if((iterModel = stateToModelCache.get(iterState)) == null)
            {
                stateToModelCache.put(iterState, RenderHelper.getModel(iterState));
                iterModel = stateToModelCache.get(iterState);
            }
            stack.push(); // Individual block placement
            stack.translate(stored.relativePos.getX(),stored.relativePos.getY() ,stored.relativePos.getZ());
            if(stored.isFluid())
            {
                vc = immediate.getBuffer(RenderLayer.getTranslucent());
                FluidState fstate = stored.getFluidState();
                FluidRenderHandler handler = FluidRenderHandlerRegistry.INSTANCE.get(fstate.getFluid());
                RenderHelper.renderBox(0, 0, 0, 1, 1, (fstate.getLevel()/8f)-0.1f, 1, 
                    handler.getFluidSprites(null, null, iterState.getFluidState())[0], 
                    vc, 
                    stack, 
                    0x00F000F0, 
                    handler.getFluidColor(ClientHelper.world.get(),
                        ClientHelper.player.get().getBlockPos(), 
                        fstate));
            }
            if(iterState.getBlock().getRenderType(iterState) != BlockRenderType.INVISIBLE)
            {
                if(((FabricBakedModel)iterModel).isVanillaAdapter())
                {
                    for(Direction dir : Direction.values())
                    {
                        if(!coords.contains(stored.relativePos.offset(dir,1)))
                        {
                            QuadInvoker.renderQuad(stack.peek(), vc, color[0], color[1], color[2], iterModel.getQuads(null, dir, random), 0xF000B0, OverlayTexture.DEFAULT_UV);
                        }
                    }
                    QuadInvoker.renderQuad(stack.peek(), vc, color[0], color[1], color[2], iterModel.getQuads(null, null, random), 0xF000B0, OverlayTexture.DEFAULT_UV);
                }
                else
                {
                    ClientHelper.blockModelRenderer.render(player.world, iterModel, iterState, BlockPos.ORIGIN, stack, vc, false, random, 42L, OverlayTexture.DEFAULT_UV);
                }

            }
            BlockEntityType<?> type = null;
            if(stored.isBlockEntity()) 
            {
                type = stored.getEntityType();
                if(BETypeToBECache.get(type) == null) cacheBlockEntity(type, stored);
            }
            BlockEntityRenderer<BlockEntity> renderer = type == null ? null : BlockEntityRenderDispatcher.INSTANCE.get(BETypeToBECache.get(type));
            if(stored.isBlockEntity() && renderer != null && type != BlockEntityType.MOB_SPAWNER)
            {
                renderer.render(BETypeToBECache.get(type), delta, stack, immediate, 0x00F000F0, OverlayTexture.DEFAULT_UV);
            }
            stack.pop();
        });
    }

    public void cacheBlockEntity(BlockEntityType<?> type,BlockStorage storage)
    {

        if(BETypeToBECache.get(type) == null)
        {
            BlockEntity be = type.instantiate();
            be.fromTag(storage.entityTag);
            ((BlockEntityAccessor)be).setCachedState(storage.state);
            ((BlockEntityAccessor)be).setWorld(ClientHelper.world.get());
            BETypeToBECache.put(type, be);
        }
    }

    /**
     * Skips rendering some blocks for performance or to ignore issues
     * @param state
     * @return
     */
    public boolean shouldIgnore(BlockState state)
    {
        if(state.isAir())return true;
        if(state.getBlock() instanceof BedBlock && state.get(BedBlock.PART) == BedPart.HEAD) return true;
        return false;
    }

    public int getXMid()
    {
        return this.width/2;
    }
    public int getYMid()
    {
        return this.height/2;
    }

    @Override
    public void onClose() {
        stateToModelCache.clear();
        BETypeToBECache.clear();
        coords.clear();
        // blocks.clear();
        super.onClose();
    }
    
}