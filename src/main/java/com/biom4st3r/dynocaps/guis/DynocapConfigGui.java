package com.biom4st3r.dynocaps.guis;

import com.biom4st3r.biow0rks.helpers.client.ClientHelper;
import com.biom4st3r.dynocaps.ModInit;
import com.biom4st3r.dynocaps.Packets;
import com.biom4st3r.dynocaps.components.IDynocapComponent;
import com.biom4st3r.dynocaps.guis.widgets.HexTextField;
import com.biom4st3r.dynocaps.guis.widgets.WBioGridPanel;

import io.github.cottonmc.cotton.gui.client.BackgroundPainter;
import io.github.cottonmc.cotton.gui.client.LightweightGuiDescription;
import io.github.cottonmc.cotton.gui.client.ScreenDrawing;
import io.github.cottonmc.cotton.gui.widget.WLabel;
import io.github.cottonmc.cotton.gui.widget.WSlider;
import io.github.cottonmc.cotton.gui.widget.WTextField;
import io.github.cottonmc.cotton.gui.widget.data.Axis;
import nerdhub.cardinal.components.api.component.ComponentProvider;
import net.minecraft.client.MinecraftClient;
import net.minecraft.item.ItemStack;
import net.minecraft.text.LiteralText;

/**
 * DynocapConfigScreen
 */

public class DynocapConfigGui extends LightweightGuiDescription {

    public static final BackgroundPainter bg = (x,y,panel)->
    {
        ScreenDrawing.drawBeveledPanel(x-1, y-1, panel.getWidth()+2, panel.getHeight()+2);
    };

    public static int maxWidth,maxHeight,maxDepth;
    IDynocapComponent component;

    private String getHex(int i)
    {
        String output = "";
        String string = Integer.toHexString(i);
        while(output.length() + string.length() < 6)
        {
            output+="0";
        }
        return String.format("%s%s", output,string).toUpperCase();

    }

    public DynocapConfigGui()
    {
        MinecraftClient.getInstance().getNetworkHandler().sendPacket(Packets.CLIENT.requestMaxValues());
        ItemStack stack = ClientHelper.player.get().getMainHandStack();
        this.component = ComponentProvider.fromItemStack(stack).getComponent(ModInit.dynocapType);
        // AbstractFurnaceContainer
        
        WBioGridPanel root = new WBioGridPanel(22);
        this.setRootPanel(root);
        WLabel title = new WLabel("Dynocap");
        root.add(title, 0, 0);

        WLabel colorTitle = new WLabel("Color:");
        root.add(colorTitle,0,1);

        WTextField colorField = new HexTextField(new LiteralText(getHex(component.getColor()))).setEditable(true);
        colorField.setSize(6*18, 20);
        colorField.setMaxLength(6);
        String textString = Integer.toHexString(component.getColor());
        while(textString.length()<6) textString = "0"+textString;
        colorField.setText(textString.toUpperCase());
        colorField.setChangedListener((value)->
        {
            if(Integer.parseInt(value,16) == component.getColor()) return;
            component.setColor(Integer.parseInt(value,16));
            colorField.setEnabledColor(Integer.parseInt(value, 16));
            syncComp(component);
        });
        root.add(colorField, 2, 1, 2, 1);



        WSlider widthSlider = new WSlider(1, maxWidth, Axis.VERTICAL);
            widthSlider.setValue(component.getWidth());
            root.add(widthSlider, 1, 4);
        WLabel widthLabel = new WLabel(widthSlider.getValue()+"");
            root.add(widthLabel,0,4);
        WLabel widthTitle = new WLabel("Width");
            root.add(widthTitle, 0,3);
        widthSlider.setValueChangeListener((i)->
        {
            widthLabel.setText(new LiteralText(i+""));
            component.setWidth(i);
            syncComp(component);
        });
        
        
        WSlider heightSlider = new WSlider(1, maxHeight, Axis.VERTICAL);
            heightSlider.setValue(component.getHeight());
            root.add(heightSlider, 3, 4);
        WLabel heightLabel = new WLabel(heightSlider.getValue()+"");
            root.add(heightLabel,2,4);
        WLabel heightTitle = new WLabel("Height");
            root.add(heightTitle, 2, 3);
        heightSlider.setValueChangeListener((i)->
        {
            heightLabel.setText(new LiteralText(i+""));
            component.setHeight(i);
            syncComp(component);
        });
        
        WSlider depthSlider = new WSlider(1, maxDepth, Axis.VERTICAL);
            depthSlider.setValue(component.getDepth());
            root.add(depthSlider, 5, 4);
        WLabel depthLabel = new WLabel(depthSlider.getValue()+"");
            root.add(depthLabel,4,4);
        WLabel depthTitle = new WLabel("Depth");
            root.add(depthTitle,4,3);
        depthSlider.setValueChangeListener((i)->
        {
            depthLabel.setText(new LiteralText(i+""));
            component.setDepth(i);
            syncComp(component);
        });

        root.validate(this);
    }

    public static void syncComp(IDynocapComponent scomponent)
    {
        ModInit.logger.debug("syncComp");
        MinecraftClient.getInstance().getNetworkHandler().sendPacket(Packets.CLIENT.requestSettingsChange(scomponent));
    }

    
}