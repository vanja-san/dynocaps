package com.biom4st3r.dynocaps.guis;

import com.biom4st3r.dynocaps.LoopingInt;
import com.biom4st3r.dynocaps.guis.widgets.WBioGridPanel;

import org.lwjgl.glfw.GLFW;

import io.github.cottonmc.cotton.gui.GuiDescription;
import io.github.cottonmc.cotton.gui.client.CottonClientScreen;
import net.minecraft.text.Text;

/**
 * NoPauseCottonScreen
 */
public class NoPauseCottonScreen extends CottonClientScreen {

    public NoPauseCottonScreen(Text title, GuiDescription description) {
        super(title, description);
    }
    
    public NoPauseCottonScreen(GuiDescription description) {
        super(description);
    }
    @Override
    public boolean isPauseScreen() {
        return false;
    }


    @Override
    public boolean keyPressed(int ch, int keyCode, int modifiers) {
        GuiDescription gui = this.getDescription();
        if(ch == GLFW.GLFW_KEY_TAB && gui.getRootPanel() instanceof WBioGridPanel && ((WBioGridPanel)gui.getRootPanel()).getChildren().size() != 0)
        {
            WBioGridPanel panel = (WBioGridPanel) gui.getRootPanel();
            // int indexOfFocus = panel.getChildren().indexOf(gui.getFocus());
            LoopingInt indexOfFocus = LoopingInt.of(panel.getChildren().indexOf(gui.getFocus()), panel.getChildren().size()-1);
            while(!panel.getChildren().get(indexOfFocus.preIncrement()).canFocus() && indexOfFocus.loopCount() < 1);
            gui.requestFocus(panel.getChildren().get(indexOfFocus.get()));
            
        }
        return super.keyPressed(ch, keyCode, modifiers);
    }


    
}