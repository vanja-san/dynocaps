package com.biom4st3r.dynocaps.guis;

import com.biom4st3r.biow0rks.helpers.client.ClientHelper;
import com.biom4st3r.dynocaps.ModInit;
import com.biom4st3r.dynocaps.components.IDynocapComponent;
import com.biom4st3r.dynocaps.guis.widgets.HexTextField;
import com.biom4st3r.dynocaps.guis.widgets.UpdatingTextFeild;
import com.biom4st3r.dynocaps.guis.widgets.WBioGridPanel;

import io.github.cottonmc.cotton.gui.client.BackgroundPainter;
import io.github.cottonmc.cotton.gui.client.LightweightGuiDescription;
import io.github.cottonmc.cotton.gui.client.ScreenDrawing;
import io.github.cottonmc.cotton.gui.widget.WGridPanel;
import io.github.cottonmc.cotton.gui.widget.WLabel;
import io.github.cottonmc.cotton.gui.widget.WTextField;
import io.github.cottonmc.cotton.gui.widget.WToggleButton;
import nerdhub.cardinal.components.api.component.ComponentProvider;
import net.minecraft.item.ItemStack;
import net.minecraft.text.LiteralText;
import net.minecraft.text.TranslatableText;

/**
 * DynocapFilledGui
 */
public class DynocapFilledGui extends LightweightGuiDescription {

    public static final BackgroundPainter bg = (x,y,panel)->
    {
        ScreenDrawing.drawBeveledPanel(x-1, y-1, 400, 400);
    };
    IDynocapComponent component;
    public DynocapFilledGui()
    {
        ItemStack stack = ClientHelper.player.get().getMainHandStack();
        this.component = ComponentProvider.fromItemStack(stack).getComponent(ModInit.dynocapType);
        // World
        WBioGridPanel root = new WBioGridPanel(10);
        this.setRootPanel(root);
        this.getRootPanel().setSize(300, 150);
        WLabel title = new WLabel("DynoCap");
        WGridPanel renderPanel = new WGridPanel();
        renderPanel.setParent(root);
        renderPanel.setBackgroundPainter((x,y,panel)->
        {
            ScreenDrawing.drawGuiPanel(x+14,y-5, 190, 160,0xFF000000);
        });
        root.add(renderPanel, 10, 0);
        root.add(title, 0, 0);

        WLabel nameLabel = new WLabel("Name");
        root.add(nameLabel, 0 , 2);
        UpdatingTextFeild nameField = new UpdatingTextFeild(new LiteralText(component.getName()));
        nameField.setSize(6*18, 20);
        nameField.setChangedListener((text)->
        {
            this.component.setName(text);
            DynocapConfigGui.syncComp(component);
        });
        root.add(nameField, 0, 3,11,1);

        WLabel colorTitle = new WLabel("Color");
        root.add(colorTitle,0,6);
        // MinecraftServer

        WTextField colorField = new HexTextField(new LiteralText(getHex(component.getColor()))).setEditable(true);
        colorField.setSize(6*18, 20);
        colorField.setMaxLength(6);
        String textString = Integer.toHexString(component.getColor());
        while(textString.length()<6) textString = "0"+textString;
        colorField.setText(textString.toUpperCase());
        colorField.setChangedListener((value)->
        {
            int newcolor;
            if((newcolor = Integer.parseInt(value,16)) == component.getColor()) return;
            // DynocapComponent
            this.component.setColor(newcolor);
            colorField.setEnabledColor(newcolor);
            DynocapConfigGui.syncComp(component);
        });
        root.add(colorField, 0, 7, 5, 1);

        WToggleButton placeAir = new WToggleButton(new TranslatableText("dialog.dynocaps.can_place_air"));
        placeAir.setToggle(component.getPlaceAir());
        placeAir.setOnToggle((state)->
        {
            this.component.setPlaceAir(state);
            DynocapConfigGui.syncComp(component);
        });
        root.add(placeAir, 0, 10);

        WLabel sizeLabel = new WLabel(String.format("%sx%sx%s", this.component.getWidth(),this.component.getHeight(),this.component.getDepth()));
        root.add(sizeLabel, 0, 13);

        root.validate(this);
    }

    @Override
    public void addPainters() {
        // this.getRootPanel().setBackgroundPainter(bg);
        super.addPainters();
    }
    private String getHex(int i)
    {
        String output = "";
        String string = Integer.toHexString(i);
        while(output.length() + string.length() < 6)
        {
            output+="0";
        }
        return String.format("%s%s", output,string).toUpperCase();
    }
}