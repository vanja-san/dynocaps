package com.biom4st3r.dynocaps.guis.widgets;

import com.biom4st3r.dynocaps.ModInit;

import org.lwjgl.glfw.GLFW;

import io.github.cottonmc.cotton.gui.widget.WTextField;
import net.minecraft.text.Text;

/**
 * UpdatingTextFeild
 */
public class UpdatingTextFeild extends WTextField {
    @Override
    public void onKeyPressed(int ch, int key, int modifiers) {
        super.onKeyPressed(ch, key, modifiers);
        if (ch == GLFW.GLFW_KEY_ENTER || ch == GLFW.GLFW_KEY_KP_ENTER) {
            this.setText(this.getText());
            this.onChanged.accept(this.getText());
            this.releaseFocus();
            ModInit.logger.debug("UpdateingTextField Enter");
        }
    }

    @Override
    public void onFocusLost() {
        this.onChanged.accept(this.getText());
    }

    /**
     * @param suggestion
     */
    public UpdatingTextFeild(Text suggestion) {
        super(suggestion);
    }

    /**
     * 
     */
    public UpdatingTextFeild() {
    }
    
}