package com.biom4st3r.dynocaps.guis.widgets;

import java.util.List;

import io.github.cottonmc.cotton.gui.widget.WGridPanel;
import io.github.cottonmc.cotton.gui.widget.WWidget;

/**
 * WGridPanel
 */
public class WBioGridPanel extends WGridPanel {
    public List<WWidget> getChildren()
    {
        return this.children;
    }

    /**
     * 
     */
    public WBioGridPanel() {
    }

    /**
     * @param gridSize
     */
    public WBioGridPanel(int gridSize) {
        super(gridSize);
    }
    
}