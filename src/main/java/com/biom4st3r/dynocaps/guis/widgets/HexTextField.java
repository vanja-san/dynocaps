package com.biom4st3r.dynocaps.guis.widgets;

import net.minecraft.text.Text;

/**
 * FriendlyTextField
 */
public class HexTextField extends UpdatingTextFeild {

    // char[] hexChars = new char[]{
    //     '0','1','2','3','4','5',
    //     '6','7','8','9','0','a',
    //     'b','c','d','e','f','A',
    //     'B','C','D','E','F'};
    String hexChars = "0123456789abcdefABCDEF";

    @Override
    public void onCharTyped(char ch) {
        if (this.text.length()<this.maxLength && hexChars.contains(ch+"")){
			//snap cursor into bounds if it went astray
			if (cursor<0) cursor=0;
			if (cursor>this.text.length()) cursor = this.text.length();
			
			String before = this.text.substring(0, cursor);
			String after = this.text.substring(cursor, this.text.length());
			this.text = before+ch+after;
			cursor++;
		}
    }

    /**
     * @param suggestion
     */
    public HexTextField(Text suggestion) {
        super(suggestion);
    }

    /**
     * 
     */
    public HexTextField() {
    }

    
}