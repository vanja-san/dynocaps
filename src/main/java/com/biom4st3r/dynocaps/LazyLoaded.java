package com.biom4st3r.dynocaps;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * This object is lazy loaded though a getter or other means
 */
@Retention(RetentionPolicy.CLASS)

public @interface LazyLoaded {

    
}