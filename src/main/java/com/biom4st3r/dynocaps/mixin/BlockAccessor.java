package com.biom4st3r.dynocaps.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.gen.Accessor;

import net.minecraft.block.Block;

/**
 * BlockAccessor
 */
@Mixin(Block.class)
public interface BlockAccessor {

    @Accessor("collidable")
    public boolean isCollidable();
    
}