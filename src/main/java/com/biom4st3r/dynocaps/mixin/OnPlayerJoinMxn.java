package com.biom4st3r.dynocaps.mixin;

import com.biom4st3r.dynocaps.ModInit;
import com.biom4st3r.dynocaps.Packets;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import net.minecraft.network.ClientConnection;
import net.minecraft.server.PlayerManager;
import net.minecraft.server.network.ServerPlayerEntity;

/**
 * WorldRendererMixin
 */
@Mixin(PlayerManager.class)
public abstract class OnPlayerJoinMxn {

    @Inject(
        method = "onPlayerConnect", 
        at = @At("RETURN"))
    private void updateDynocapSettings(ClientConnection connection, ServerPlayerEntity player, CallbackInfo ci)
    {
        player.networkHandler.sendPacket(Packets.SERVER.sendMaxValues(ModInit.config.maxWidth, ModInit.config.maxHeight, ModInit.config.maxDepth));
    }



    
}