package com.biom4st3r.dynocaps.mixin;

import com.biom4st3r.dynocaps.CapCaseModel;
import com.biom4st3r.dynocaps.ModInit;
import com.biom4st3r.dynocaps.ModInitClient;

import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import it.unimi.dsi.fastutil.ints.Int2ObjectMap;
import net.minecraft.client.render.item.ItemModels;
import net.minecraft.client.render.model.BakedModel;
import net.minecraft.item.Item;

/**
 * SlashScreenInitMixin
 */
@Mixin(ItemModels.class)
public abstract class ItemModelsMixin {

    @Shadow @Final
    private Int2ObjectMap<BakedModel> models;

    // MinecraftClient
    @Inject(method = "reloadModels",at = @At("RETURN"))
    public void lateReg(CallbackInfo ci)
    {
        ModInitClient.capCase = models.get(Item.getRawId(ModInit.CapCase));
        models.replace(Item.getRawId(ModInit.CapCase), ModInitClient.capCase, new CapCaseModel());
    }
    
}