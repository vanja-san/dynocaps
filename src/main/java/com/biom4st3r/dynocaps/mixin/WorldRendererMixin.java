package com.biom4st3r.dynocaps.mixin;

import com.biom4st3r.dynocaps.features.BoxRenderer;

import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.At.Shift;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import net.minecraft.client.render.BufferBuilderStorage;
import net.minecraft.client.render.Camera;
import net.minecraft.client.render.GameRenderer;
import net.minecraft.client.render.LightmapTextureManager;
import net.minecraft.client.render.WorldRenderer;
import net.minecraft.client.util.math.Matrix4f;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.client.world.ClientWorld;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.profiler.Profiler;

/**
 * WorldRendererMixin
 */
@Mixin(WorldRenderer.class)
public abstract class WorldRendererMixin {

    @Shadow
    @Final
    private BufferBuilderStorage bufferBuilders;

    @Shadow
    private ClientWorld world;

    @Inject(
        method = "render", 
        at = @At(
            value = "INVOKE_STRING",
            target = "net/minecraft/util/profiler/Profiler.swap(Ljava/lang/String;)V",
            args = "ldc=blockentities",shift = Shift.BEFORE))
    private void myrenderer(MatrixStack stack, float tickDelta, long limitTime, 
        boolean renderBlockOutline, Camera camera, GameRenderer gameRenderer, 
        LightmapTextureManager lightmapTextureManager, Matrix4f matrix4f, CallbackInfo ci)
    {        
        Profiler profiler = world.getProfiler();
        Vec3d camPos = camera.getPos();
        profiler.swap("dynocaps");
        
        BoxRenderer.render(stack, bufferBuilders.getEntityVertexConsumers(), 1, camPos);
    }



    
}