package com.biom4st3r.dynocaps.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.gen.Accessor;

import net.minecraft.block.BlockState;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.world.World;

/**
 * BlockEntityAccessor
 */
@Mixin(BlockEntity.class)
public interface BlockEntityAccessor {

    @Accessor("cachedState")
    public void setCachedState(BlockState state);
    @Accessor("world")
    public void setWorld(World world);
    
}