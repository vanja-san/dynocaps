package com.biom4st3r.dynocaps;

import java.util.function.Supplier;

import net.minecraft.block.Block;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;

public final class Register 
{
    public static void setMODID(String string)
    {
        MODID = string;
    }
    private static String MODID = "biow0rks";
    public static <T extends Item> T regItem(String string, T item)
    {
        return Registry.register(Registry.ITEM, new Identifier(MODID, string), item);
    }
    public static <T extends Block> T regBlock(String string, T block)
    {
        return Registry.register(Registry.BLOCK, new Identifier(MODID, string), block);
    }
    public static <T extends BlockEntityType<?>> T regBE(String string, T be)
    {
        return Registry.register(Registry.BLOCK_ENTITY_TYPE, new Identifier(MODID, string), be);
    }
    public static BlockItem regBlockItem(String string,Block block, Item.Settings item)
    {
        return Registry.register(Registry.ITEM, new Identifier(MODID, string), new BlockItem(block, item));
    }
    public static <T extends BlockEntity> BlockEntityType<T> regBE(String string, Supplier<T> supplier,Block... supportedBlocks)
    {
        return Registry.register(Registry.BLOCK_ENTITY_TYPE, new Identifier(MODID, string), BlockEntityType.Builder.create(supplier, supportedBlocks).build(null));
    }
}