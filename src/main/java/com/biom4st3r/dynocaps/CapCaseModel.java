package com.biom4st3r.dynocaps;

import java.util.List;
import java.util.Random;
import java.util.function.Supplier;

import com.biom4st3r.biow0rks.helpers.client.RenderHelper;
import com.google.common.collect.Lists;

import net.fabricmc.fabric.api.renderer.v1.model.FabricBakedModel;
import net.fabricmc.fabric.api.renderer.v1.render.RenderContext;
import net.minecraft.block.BlockState;
import net.minecraft.client.render.model.BakedModel;
import net.minecraft.client.render.model.BakedQuad;
import net.minecraft.client.render.model.json.ModelItemPropertyOverrideList;
import net.minecraft.client.render.model.json.ModelTransformation;
import net.minecraft.client.texture.Sprite;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Identifier;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.world.BlockRenderView;

/**
 * CapBoxModel
 */
public class CapCaseModel implements FabricBakedModel, BakedModel {

    @Override
    public List<BakedQuad> getQuads(BlockState state, Direction face, Random random) {
        return Lists.newArrayList();
    }

    @Override
    public boolean useAmbientOcclusion() {
        return false;
    }

    @Override
    public boolean hasDepth() {
        return false;
    }

    @Override
    public boolean isSideLit() {
        return false;
    }

    @Override
    public boolean isBuiltin() {
        return false;
    }

    @Override
    public Sprite getSprite() {
        return ModInitClient.capCase.getSprite();
    }

    @Override
    public ModelTransformation getTransformation() {
        return ModInitClient.capCase.getTransformation();
    }

    @Override
    public ModelItemPropertyOverrideList getItemPropertyOverrides() {
        return ModInitClient.capCase.getItemPropertyOverrides();
    }

    @Override
    public boolean isVanillaAdapter() {
        return false;
    }

    @Override
    public void emitBlockQuads(BlockRenderView blockView, BlockState state, BlockPos pos,
            Supplier<Random> randomSupplier, RenderContext context) {

    }
    @LazyLoaded
    public static Sprite[] sprites = null;
    @Override
    public void emitItemQuads(ItemStack stack, Supplier<Random> randomSupplier, RenderContext context) {
        if(sprites == null)
        {
            sprites = new Sprite[]{
                RenderHelper.getBlockSprite(new Identifier(ModInit.MODID, "items/cap_case/cap_case0")),
                RenderHelper.getBlockSprite(new Identifier(ModInit.MODID, "items/cap_case/cap_case1")),
                RenderHelper.getBlockSprite(new Identifier(ModInit.MODID, "items/cap_case/cap_case2")),
                RenderHelper.getBlockSprite(new Identifier(ModInit.MODID, "items/cap_case/cap_case3")),
                RenderHelper.getBlockSprite(new Identifier(ModInit.MODID, "items/cap_case/cap_case4")),
                RenderHelper.getBlockSprite(new Identifier(ModInit.MODID, "items/cap_case/cap_case5")),
                RenderHelper.getBlockSprite(new Identifier(ModInit.MODID, "items/cap_case/cap_case6")),
                RenderHelper.getBlockSprite(new Identifier(ModInit.MODID, "items/cap_case/cap_case7"))
                };
        }


        // QuadEmitter emitter = context.getEmitter();
        // Inventory inv = ModInit.inventoryType.get(stack);
        // BakedModel model = ModInitClient.capCase;
        // BakedQuad quad = model.getQuads(null, null, null).get(0); // proper quad
        // emitter.fromVanilla(quad.getVertexData(), 0, true).emit();
        // emitter.square(Direction.SOUTH, 0, 0, 1, 1, 0)
        //     .spriteBake(0, model.getSprite(), 0)
        //     .emit();
        // emitter.square(Direction.SOUTH, 0, 0, 1, 1, 0.5f)
        //     .spriteBake(0, sprites[0], 0)
        //     .spriteColor(0, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF)
        //     .cullFace(Direction.SOUTH)
        //     .emit();

        // for(int i = 0; i < inv.getInvSize();i++)
        // {
        //     if(inv.getInvStack(i).isEmpty()) continue;
        //     int color = 0xFf000000 | ModInit.dynocapType.get(inv.getInvStack(i)).getColor();
        //     // System.out.println(Integer.toHexString(color));
        //     Sprite sprite = sprites[i];
        //     emitter.square(Direction.SOUTH, 0, 0, 1, 1, 0.46f)
                
        //         .spriteBake(0, sprite, 0)
        //         .spriteColor(0, color,color,color,color)
        //         .emit();
        // }
        context.fallbackConsumer().accept(ModInitClient.capCase);
        

    }

    
}