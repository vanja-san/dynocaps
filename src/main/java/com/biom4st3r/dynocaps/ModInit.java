package com.biom4st3r.dynocaps;

import java.util.Set;
import java.util.SortedMap;

import com.biom4st3r.biow0rks.BioLogger;
import com.biom4st3r.biow0rks.helpers.client.ClientHelper;
import com.biom4st3r.coderecipes.api.RecipeHelper;
import com.biom4st3r.coderecipes.api.Recipes;
import com.biom4st3r.dynocaps.components.DynocapComponent;
import com.biom4st3r.dynocaps.components.IDynocapComponent;
import com.biom4st3r.dynocaps.components.IInventoryComponent;
import com.biom4st3r.dynocaps.components.InventoryComponent;
import com.biom4st3r.dynocaps.guis.CapCaseController;
import com.biom4st3r.dynocaps.guis.DynocapConfigGui;
import com.biom4st3r.dynocaps.guis.DynocapFilledGui;
import com.biom4st3r.dynocaps.guis.DynocapFilledScreen;
import com.biom4st3r.dynocaps.guis.NoPauseCottonScreen;
import com.biom4st3r.dynocaps.items.DynoCapCaseItem;
import com.biom4st3r.dynocaps.items.DynocapItem;
import com.google.common.collect.Sets;

import io.github.cottonmc.cotton.config.ConfigManager;
import nerdhub.cardinal.components.api.ComponentRegistry;
import nerdhub.cardinal.components.api.ComponentType;
import nerdhub.cardinal.components.api.event.ItemComponentCallback;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.client.itemgroup.FabricItemGroupBuilder;
import net.fabricmc.fabric.api.container.ContainerProviderRegistry;
import net.fabricmc.fabric.api.event.server.ServerStartCallback;
import net.minecraft.block.Block;
import net.minecraft.block.Blocks;
import net.minecraft.client.render.BufferBuilder;
import net.minecraft.client.render.RenderLayer;
import net.minecraft.item.Item;
import net.minecraft.item.Item.Settings;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.recipe.RecipeType;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;

public class ModInit implements ModInitializer {
	public static final String MODID = "dynocaps";
	public static BioLogger logger = new BioLogger(MODID);
	public static SortedMap<RenderLayer, BufferBuilder> entityBuilders;
	public static Config config = ConfigManager.loadConfig(Config.class);
	public static Set<Block> preventCopyBlocks;
	public static Set<Block> preventOverrideBlocks;
	public static Identifier containerid = new Identifier(MODID, "capcasecontainer");

	public static final ComponentType<IDynocapComponent> dynocapType = ComponentRegistry.INSTANCE
			.registerIfAbsent(new Identifier(MODID, "dynocap"), IDynocapComponent.class);
	public static final ComponentType<IInventoryComponent> inventoryType = ComponentRegistry.INSTANCE
			.registerIfAbsent(new Identifier(MODID, "inventory_component"),IInventoryComponent.class);
	public static Item DynoCap;
	public static Item DynoCap_Top;
	public static Item DynoCap_Base;
	public static Item CapCase;

	@Environment(EnvType.CLIENT)
	public static void openGUI(boolean filled)
	{
		if (!filled) {
			ClientHelper.client.openScreen(new NoPauseCottonScreen(new DynocapConfigGui()));
		} else {
			ClientHelper.client.openScreen(new DynocapFilledScreen(new DynocapFilledGui()));
		}
	}

	@Override
	public void onInitialize() {
		Packets.SERVER.init();
		Register.setMODID(MODID);
		DynoCap = Register.regItem("basiccap", new DynocapItem());
		DynoCap_Top = Register.regItem("compressor", new Item(new Settings().group(DYNO_GROUP).maxCount(8)));
		DynoCap_Base = Register.regItem("decompressor", new Item(new Settings().group(DYNO_GROUP).maxCount(8)));
		CapCase = Register.regItem("cap_case", new DynoCapCaseItem());
		ItemComponentCallback.event(DynoCap).register((stack,list)->
		{
			list.put(dynocapType, new DynocapComponent());
		});
		ItemComponentCallback.event(CapCase).register((stack,list)->
		{
			list.put(inventoryType, new InventoryComponent());
		});
		ServerStartCallback.EVENT.register((server)->
		{
			preventCopyBlocks = Sets.newHashSet();
			preventOverrideBlocks = Sets.newHashSet();
			for(String string : config.preventCopyBlocks)
			{
				Block block = Registry.BLOCK.get(new Identifier(string));
				if(block == Blocks.AIR)
				{
					logger.log("%s was not found for preventCopyBlocks", string);
				}
				else
				{
					preventCopyBlocks.add(block);
					logger.log("%s was added to preventCopyBlocks", string);
				}
			}
			for(String string : config.preventOverrideBlocks)
			{
				Block block = Registry.BLOCK.get(new Identifier(string));
				if(block == Blocks.AIR)
				{
					logger.log("%s was not found for preventOverrideBlocks", string);
				}
				else
				{
					preventOverrideBlocks.add(block);
					logger.log("%s was added to preventOverrideBlocks", string);
				}
			}
		});
		Identifier id;
		RecipeHelper.addRecipeToMap((id = new Identifier(MODID, "dynocap_recipe")), RecipeType.CRAFTING, Recipes.newShapedRecipe(3, 3)
			.addIngredient2(DynoCap_Top)
			.addIngredient5(Items.ENDER_EYE)
			.addIngredient8(DynoCap_Base)
			.build(id, id.toString(), new ItemStack(DynoCap)));
		RecipeHelper.addRecipeToMap((id=new Identifier(MODID, "dynocap_compressor_recipe")), RecipeType.CRAFTING, Recipes.newShapedRecipe(3, 3)
			.addIngredient1(Items.IRON_BLOCK).addIngredient2(Items.IRON_BLOCK).addIngredient3(Items.IRON_BLOCK)
			.addIngredient4(Items.PISTON,Items.STICKY_PISTON).addIngredient5(Items.PISTON,Items.STICKY_PISTON).addIngredient6(Items.PISTON,Items.STICKY_PISTON)
			.addIngredient7(Items.IRON_BLOCK).addIngredient8(Items.IRON_INGOT).addIngredient9(Items.IRON_BLOCK)
			.build(id, id.toString(), new ItemStack(DynoCap_Top)));
		RecipeHelper.addRecipeToMap((id=new Identifier(MODID, "dynocap_decompressor_recipe")), RecipeType.CRAFTING, Recipes.newShapedRecipe(3, 3)
			.addIngredient1(Items.IRON_BLOCK)                   .addIngredient3(Items.IRON_BLOCK)
			.addIngredient4(Items.IRON_BLOCK).addIngredient5(Items.CHEST).addIngredient6(Items.IRON_BLOCK)
			.addIngredient7(Items.IRON_BLOCK).addIngredient8(Items.IRON_BLOCK).addIngredient9(Items.IRON_BLOCK)
			.build(id, id.toString(), new ItemStack(DynoCap_Base)));
		RecipeHelper.addRecipeToMap((id=new Identifier(MODID, "dynocap_capbxo_recipe")), RecipeType.CRAFTING, Recipes.newShapedRecipe(3, 3)
			.addIngredient2(Items.IRON_INGOT)
			.addIngredient5(Items.WHITE_WOOL,Items.BLACK_WOOL,Items.GRAY_WOOL,Items.LIGHT_GRAY_WOOL)
			.addIngredient8(Items.IRON_INGOT)
			.build(id, id.toString(), new ItemStack(CapCase)));
		ContainerProviderRegistry.INSTANCE.registerFactory(containerid, (syncId, ident, player, buf)->
		{
			return new CapCaseController(syncId, player.inventory,buf.readByte());
		});
	}
	public static final ItemGroup DYNO_GROUP = FabricItemGroupBuilder.build(new Identifier(MODID, "dynocap_tab"), ()->new ItemStack(DynoCap));

}
