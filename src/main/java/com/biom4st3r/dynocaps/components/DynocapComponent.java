package com.biom4st3r.dynocaps.components;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import com.biom4st3r.biow0rks.helpers.WorldHelper;
import com.biom4st3r.dynocaps.BlockStorage;
import com.biom4st3r.dynocaps.ModInit;
import com.google.common.collect.Lists;

import nerdhub.cardinal.components.api.ComponentType;
import nerdhub.cardinal.components.api.component.Component;
import nerdhub.cardinal.components.api.util.ItemComponent;
import net.minecraft.block.Blocks;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.block.entity.LootableContainerBlockEntity;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.ListTag;
import net.minecraft.particle.ParticleTypes;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.sound.SoundCategory;
import net.minecraft.sound.SoundEvents;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.Clearable;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Box;
import net.minecraft.util.math.Vec3i;
import net.minecraft.world.World;

/**
 * DynocapComponent
 */
public class DynocapComponent implements IDynocapComponent, ItemComponent<IDynocapComponent> {

    int width = 1, depth = 1, height = 1;
    List<BlockStorage> storage = Lists.newArrayList();
    int hashcode = -1;
    int color = 0xFF7545;
    boolean shouldPlaceAir = true;
    String name = new TranslatableText(ModInit.DynoCap.getTranslationKey()).asFormattedString();
    public DynocapComponent() {
        storage = Lists.newArrayList();
        width = 5;
        depth = 5;
        height = 5;
        hashcode = -1;
    }

    @Override
    public int getWidth() {
        return width;
    }

    @Override
    public int getHeight() {
        return height;
    }

    @Override
    public int getDepth() {
        return depth;
    }

    // public boolean asyncIsFilled()
    // {
    //     if(asyncStorage == null) return false;
    //     if(asyncStorage.isDone()) return true;
    //     return false;
    // }

    // private CompletableFuture<Set<BlockStorage>> asyncStorage;

    // private boolean Async_getBoxCotents(World world, Box box, boolean destroy)
    // {
    //     if(world == null || box == null || asyncStorage == null || !asyncStorage.isDone())
    //     {
    //         return false;
    //     }
    //     this.asyncStorage = ((ServerWorld)world).getServer().submit(()->
    //     {
    //         Set<BlockStorage> store = Sets.newHashSet();
    //         for (int y = getHeight() - 1; y >= 0; y--) {
    //             for (int x = 0; x < getWidth(); x++) {
    //                 for (int z = 0; z < getDepth(); z++) {
    //                     BlockPos pos = new BlockPos(box.x1 + x, box.y1 + y, box.z1 + z);
    
    //                     store.add(BlockStorage.newBlockStorage(world, pos, new Vec3i(x, y, z)));
    //                     // ChestBlockEntity
    //                     if (destroy) {
    //                         BlockEntity be;
    //                         if ((be = world.getBlockEntity(pos)) != null) {
    //                             if (be instanceof Clearable) {
    //                                 ((Clearable) be).clear();
    //                             }
    //                         }
    //                         world.setBlockState(pos, Blocks.AIR.getDefaultState(),0b110011);
    //                     }
    //                 }
    //             }
    //         }
    //         return store;
    //     });
    //     return true;
    // }

    /**
     * flag 0b110011 is used to stop blocks from changing as their neighbors are removed such as stairs or fences unlinking<p>
     * TODO remove box and replace with {@link #BlockPos}
     */
    @Override
    public List<BlockStorage> getBoxContents(World world, Box box, boolean destroy) {
        if(world == null || box == null)
        {
            return storage;
        }
        for (int y = getHeight() - 1; y >= 0; y--) {
            for (int x = 0; x < getWidth(); x++) {
                for (int z = 0; z < getDepth(); z++) {
                    BlockPos pos = new BlockPos(box.x1 + x, box.y1 + y, box.z1 + z);
                    if(ModInit.preventCopyBlocks.contains(world.getBlockState(pos).getBlock())) 
                    {
                        BlockStorage bs = new BlockStorage();
                        bs.entityTag= null;
                        bs.relativePos = (new Vec3i(x,y,z));
                        bs.state = Blocks.AIR.getDefaultState();
                        storage.add(bs);
                        continue;
                    }
                    storage.add(BlockStorage.newBlockStorage(world, pos, new Vec3i(x, y, z)));
                    if (destroy) {
                        BlockEntity be;
                        if ((be = world.getBlockEntity(pos)) != null) {
                            if(be instanceof LootableContainerBlockEntity)
                            {
                                ((LootableContainerBlockEntity)be).checkLootInteraction(null);
                            }
                            if (be instanceof Clearable) {
                                ((Clearable) be).clear();
                            }

                        }
                        world.setBlockState(pos, Blocks.AIR.getDefaultState(),0b110111);
                    }
                }
            }
        }
        Collections.reverse(storage);
        hashcode = storage.hashCode();
        return storage;
    }



    @Override
    public Result releaseBlocks(World world, BlockPos pos) {
        Result result = Result.SUCCESS;
        Iterator<BlockStorage> iter = storage.iterator();
        if(ModInit.config.requireEmptyArea && WorldHelper.getBlocksInBox(world, new Box(pos, pos.add(new Vec3i(this.width-1, this.height-1, this.depth-1)))).stream().anyMatch((blockstate)->!blockstate.isAir()))
        {
            return Result.PREVENT_OVERRIDES;
        }
        while(iter.hasNext())
        {
            BlockStorage store = iter.next();
            if(!this.shouldPlaceAir && store.state.isAir()) 
            {
                iter.remove();
                continue;
            }
            BlockPos copy = BlockPos.fromLong(pos.asLong()).add(store.relativePos);
            if(ModInit.preventOverrideBlocks.contains(world.getBlockState(copy).getBlock())) {
                result = Result.PROHIBIITED_BLOCK;
                continue;
            }
            if(world.getDimension().doesWaterVaporize() && store.state.getBlock() == Blocks.WATER)
            {
                world.setBlockState(copy, Blocks.AIR.getDefaultState());
                world.playSound(null, pos, SoundEvents.BLOCK_FIRE_EXTINGUISH, SoundCategory.BLOCKS, 0.5F, 2.6F + (world.random.nextFloat() - world.random.nextFloat()) * 0.8F);

               for(int l = 0; l < 8; ++l) {
                  world.addParticle(ParticleTypes.LARGE_SMOKE, (double)copy.getX() + Math.random(), (double)copy.getY() + Math.random(), (double)copy.getZ() + Math.random(), 0.0D, 0.0D, 0.0D);
               }
            }
            else
            {
                world.setBlockState(copy, store.state); 
            }
            spawnParticles(world, copy);
            if (store.isBlockEntity()) {
                if (world.getBlockEntity(copy).getType() == BlockEntityType.PISTON)
                    continue;
                
                store.entityTag.putInt("x", copy.getX());
                store.entityTag.putInt("y", copy.getY());
                store.entityTag.putInt("z", copy.getZ());
                world.getBlockEntity(copy).fromTag(store.entityTag);
            }
            iter.remove();
        }
        if(result==Result.SUCCESS)
        {
            world.playSound(pos.getX(), pos.getY(), pos.getZ(), SoundEvents.ENTITY_GENERIC_EXPLODE, SoundCategory.BLOCKS, 4.0F, 0f, false);
        }
        return result;
    }
    
    private void spawnParticles(World world, BlockPos copy) {
        if (world.random.nextInt(2) == 0) {
            ((ServerWorld) world).spawnParticles(ParticleTypes.EXPLOSION, copy.getX(), copy.getY(), copy.getZ(), 1,
                    world.random.nextFloat() - 0.5f, world.random.nextFloat() - 0.5f, world.random.nextFloat() - 0.5f,
                    world.random.nextFloat() - 0.5);
        } else {
            ((ServerWorld) world).spawnParticles(ParticleTypes.POOF, copy.getX(), copy.getY(), copy.getZ(), 5,
                    world.random.nextFloat() - 0.5f, world.random.nextFloat() - 0.5f, world.random.nextFloat() - 0.5f,
                    world.random.nextFloat() - 0.5);
        }
    };

    @Override
    public void setWidth(int i) {
        width = i;

    }

    @Override
    public void setHeight(int i) {
        height = i;

    }

    @Override
    public void setDepth(int i) {
        depth = i;

    }

    public static final String HASH = "a", LIST = "c", X = "x", Y = "y", Z = "z",COLOR = "d",PLACE_AIR = "e",NAME="f";

    @Override
    public CompoundTag toTag(CompoundTag tag) {
        tag.putBoolean(PLACE_AIR, this.shouldPlaceAir);
        tag.putInt(COLOR, this.getColor());
        tag.putShort(X, (short) getWidth());
        tag.putShort(Y, (short) getHeight());
        tag.putShort(Z, (short) getDepth());
        tag.putInt(HASH, hashcode);
        tag.putString(NAME, name);
        ListTag lt = new ListTag();
        for (BlockStorage storage : storage) {
            lt.add(storage.toTag(new CompoundTag()));
        }
        tag.put(LIST, lt);
        return tag;
    }

    @Override
    public void fromTag(CompoundTag tag) {
        width = tag.getShort(X);
        height = tag.getShort(Y);
        depth = tag.getShort(Z);
        hashcode = tag.getInt(HASH);
        storage = Lists.newArrayList();
        color = tag.getInt(COLOR);
        shouldPlaceAir = tag.getBoolean(PLACE_AIR);
        this.name = tag.getString(NAME);
        ListTag lt = (ListTag) tag.get(LIST);
        if (lt == null)
            lt = new ListTag();
        storage.clear();
        lt.forEach((rawtag) -> {
            storage.add(new BlockStorage().fromTag((CompoundTag) rawtag));
        });
    }

    @Override
    public boolean isFilled() {
        return storage.size() > 0;
    }

    @Override
    public void clearStorage() {
        hashcode = -1;
        storage.clear();
    }

    @Override
    public boolean isComponentEqual(Component other) {
        if (other instanceof DynocapComponent) {
            DynocapComponent o = (DynocapComponent) other;
            if (this.storage.size() == o.storage.size()) {
                return this.hashcode == o.hashcode;
            }
        }
        return false;
    }

    @Override
    public ComponentType<IDynocapComponent> getComponentType() {
        return ModInit.dynocapType;
    }

    @Override
    public int getColor() {
        return color;
    }

    @Override
    public void setColor(int i) {
        this.color = i;
    }

    @Override
    public boolean getPlaceAir() {
        return shouldPlaceAir;
    }

    @Override
    public void setPlaceAir(boolean bl) {
        this.shouldPlaceAir = bl;

    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String s) {
        this.name = s;

    }
    
}