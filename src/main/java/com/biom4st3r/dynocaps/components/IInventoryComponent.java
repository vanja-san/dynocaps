package com.biom4st3r.dynocaps.components;

import com.biom4st3r.dynocaps.ModInit;

import nerdhub.cardinal.components.api.component.Component;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.Inventory;
import net.minecraft.item.ItemStack;

/**
 * InventoryComponent
 */
public interface IInventoryComponent extends Component, Inventory {

    @Override
    default boolean canPlayerUseInv(PlayerEntity player) {
        return true;
    }

    @Override
    default int getInvSize() {
        return 4;
    }

    @Override
    default boolean isValidInvStack(int slot, ItemStack stack) {
        return stack.getItem() == ModInit.DynoCap;
    }







    
}