package com.biom4st3r.dynocaps.components;

import java.util.List;

import com.biom4st3r.dynocaps.ModInit;
import com.google.common.collect.Lists;

import nerdhub.cardinal.components.api.ComponentType;
import nerdhub.cardinal.components.api.component.Component;
import nerdhub.cardinal.components.api.util.ItemComponent;
import net.minecraft.inventory.InventoryListener;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.ListTag;
import net.minecraft.util.DefaultedList;

/**
 * InventoryComponent
 */
public class InventoryComponent implements IInventoryComponent,ItemComponent<IInventoryComponent> {


    /**
     *
     */
    private static final String INVENTORY = "dynocapinv";
    private DefaultedList<ItemStack> list = DefaultedList.ofSize(this.getInvSize(), ItemStack.EMPTY);
    private List<InventoryListener> listeners = Lists.newArrayListWithCapacity(2);

    public InventoryComponent()
    {
    }



    public void addListener(InventoryListener listener)
    {
        this.listeners.add(listener);
    }

    public void removeListener(InventoryListener listener)
    {
        this.listeners.remove(listener);
    }

    @Override
    public void fromTag(CompoundTag tag) {
        list.clear();
        ListTag lt = (ListTag) tag.get(INVENTORY);
        for(int i = 0; i < lt.size(); i++)
        {
            // list.add(ItemStack.fromTag( lt.getCompound(i)));
            list.set(i, ItemStack.fromTag( lt.getCompound(i)));
        }
    }

    @Override
    public CompoundTag toTag(CompoundTag tag) {
        ListTag lt = new ListTag();
        for(ItemStack is : list)
        {
            lt.add(is.toTag(new CompoundTag()));
        }
        tag.put(INVENTORY, lt);
        return tag;
    }

    @Override
    public boolean isInvEmpty() {
        
        return list.stream().anyMatch((itemstack)->!itemstack.isEmpty());
    }

    @Override
    public ItemStack getInvStack(int slot) {
        slot = Math.min(slot, 7);
        return list.get(Math.max(0, slot));
    }

    @Override
    public ItemStack takeInvStack(int slot, int amount) {
        ItemStack value = list.get(slot);
        list.set(slot, ItemStack.EMPTY);
        return value;
    }

    @Override
    public void markDirty() {
        listeners.forEach((listener)->listener.onInvChange(this));
    }

    @Override
    public ItemStack removeInvStack(int slot) {
        return this.takeInvStack(slot, -1);
    }

    @Override
    public void setInvStack(int slot, ItemStack stack) {
        this.list.set(slot, stack);
    }

    @Override
    public void clear() {
        for(int i = 0; i < list.size(); i++)
        {
            list.set(i, ItemStack.EMPTY);
        }

    }

    @Override
    public ComponentType<IInventoryComponent> getComponentType() {
        return ModInit.inventoryType;
    }
    @Override
    public boolean isComponentEqual(Component other) {
        if(other instanceof IInventoryComponent)
        {
            boolean same = true;
            for(int i = 0; i < this.getInvSize(); i++)
            {
                same &= this.list.get(i).getItem() == ((InventoryComponent)other).list.get(i).getItem();
                if(!same) return false;
            }
            return true;
        }
        return false;
    }

    
}