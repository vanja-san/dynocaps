package com.biom4st3r.dynocaps.components;

import java.util.List;

import com.biom4st3r.dynocaps.BlockStorage;

import nerdhub.cardinal.components.api.component.Component;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Box;
import net.minecraft.world.World;

/**
 * IDynocapComponent
 */
public interface IDynocapComponent extends Component {

    public int getWidth();

    public int getHeight();

    public int getDepth();

    public void setWidth(int i);

    public void setHeight(int i);

    public void setDepth(int i);

    public List<BlockStorage> getBoxContents(World world, Box box, boolean destroy);

    public enum Result{
        SUCCESS,
        PREVENT_OVERRIDES,
        PROHIBIITED_BLOCK
    }

    public Result releaseBlocks(World world, BlockPos pos);

    public boolean isFilled();

    public void clearStorage();
    
    public int getColor();

    public void setColor(int i);

    public boolean getPlaceAir();
    public void setPlaceAir(boolean bl);

    public String getName();
    public void setName(String s);

}