package com.biom4st3r.dynocaps.features;

import com.biom4st3r.biow0rks.helpers.client.ClientHelper;
import com.biom4st3r.biow0rks.helpers.client.RenderHelper;
import com.biom4st3r.dynocaps.ModInit;
import com.biom4st3r.dynocaps.components.IDynocapComponent;

import nerdhub.cardinal.components.api.component.ComponentProvider;
import net.minecraft.client.network.ClientPlayerEntity;
import net.minecraft.client.render.RenderLayer;
import net.minecraft.client.render.VertexConsumer;
import net.minecraft.client.render.VertexConsumerProvider;
import net.minecraft.client.texture.Sprite;
import net.minecraft.client.texture.TextureManager;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.Direction;
import net.minecraft.util.math.Vec3d;

/**
 * BoxRenderer
 */
public class BoxRenderer {

    public static void render(MatrixStack stack, VertexConsumer vc,int light, Vec3d camPos)
    {
        light = 0x00F000F0;
        
        ClientPlayerEntity entity = ClientHelper.client.player;
        ItemStack hand = entity.getMainHandStack();
        if(hand.getItem() == ModInit.DynoCap)
        {
            IDynocapComponent component = ComponentProvider.fromItemStack(hand).getComponent(ModInit.dynocapType);
            stack.push();
            stack.translate(entity.getX()-camPos.x, entity.getY()-camPos.y, entity.getZ()-camPos.z);
            float boxWidth = component.getWidth();
            float boxHeight = component.getHeight();
            float boxDepth = component.getDepth();

            Direction dir = entity.getHorizontalFacing();
            Sprite sprite = RenderHelper.getBlockSprite(TextureManager.MISSING_IDENTIFIER);
            double xoff = entity.getX();
            double yoff = entity.getY();
            double zoff = entity.getZ();

            xoff = xoff - Math.floor(xoff);
            yoff = yoff - Math.floor(yoff);
            zoff = zoff - Math.floor(zoff);

            stack.translate(-xoff, -yoff+0.5f, -zoff);
            final float x = 0;
            final float y = 0;
            final float z = 0;
            int color = component.getColor();

            stack.translate(0, -0.5f, 0);
            if(dir == Direction.SOUTH)
            {
                stack.translate(-Math.floor(boxWidth/2f), 0, z+boxDepth+1);
            }
            else if(dir == Direction.EAST)
            {
                stack.translate(x+2, 0, Math.floor(boxDepth/2f) - (boxDepth%2==0 ? 1 : 0));
            }
            else if(dir == Direction.NORTH)
            {
                stack.translate(-Math.floor(boxWidth/2f), 0, z-2);
            }
            else if(dir == Direction.WEST)
            {
                stack.translate(x-boxWidth-1, y, Math.floor(boxDepth/2f) - (boxDepth%2==0 ? 1 : 0));
            }
            RenderHelper.renderBox(x, y, z, boxWidth, boxDepth, boxHeight, 1, sprite, vc, stack, light, color);
            stack.pop();
        }
    }

    public static void  render(MatrixStack stack, VertexConsumerProvider.Immediate immediate,int light, Vec3d camPos)
    {
        VertexConsumer vc = immediate.getBuffer(RenderLayer.getLines());
        render(stack, vc, light, camPos);
    }
}