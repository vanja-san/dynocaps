package com.biom4st3r.dynocaps;

import blue.endless.jankson.Comment;
import io.github.cottonmc.cotton.config.annotations.ConfigFile;

/**
 * Config
 */
@ConfigFile(name="dynocapConfig")
public class Config {

    @Comment(value="default value 10")
    public int maxWidth = 10;
    @Comment(value="default value 10")
    public int maxHeight = 10;
    @Comment(value="default value 10")
    public int maxDepth = 10;
    @Comment(value="Requires an area to be empty and prevent overriding any blocks")
    public boolean requireEmptyArea = false;
    // Blocks
    @Comment(value="Blocks that shouldn't be stored in the Dynocap")
    public String[] preventCopyBlocks = new String[]{
        "minecraft:bedrock",
        "minecraft:command_block",
        "minecraft:barrier",
        "minecraft:repeating_command_block",
        "minecraft:chain_command_block",
        "minecraft:structure_block",
        "minecraft:end_portal_frame",
        "minecraft:nether_portal"
    };
    @Comment(value="Blocks that shouldn't be replaced by placing a dynocap")
    public String[] preventOverrideBlocks = new String[]{
        "minecraft:bedrock",
        "minecraft:command_block",
        "minecraft:barrier",
        "minecraft:repeating_command_block",
        "minecraft:chain_command_block",
        "minecraft:structure_block",
        "minecraft:end_portal_frame",
        "minecraft:nether_portal"
    };
    
}