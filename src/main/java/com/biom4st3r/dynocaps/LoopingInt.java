package com.biom4st3r.dynocaps;

/**
 * OverFlowingInt
 */
public class LoopingInt {

    int value;
    int max;
    boolean looped = false;
    int loopCount = 0;
    public LoopingInt(int i,int maxInclusive)
    {
        this.value = i;
        this.max = maxInclusive;
    }
    public static LoopingInt of(int i, int maxInclusive)
    {
        return new LoopingInt(i, maxInclusive);
    }

    public int preIncrement()
    {
        if(++value > max) loop();
        return value;
    }
    public int postIncrement()
    {
        int i = value;
        if(++value > max) loop();
        return i;
    }
    public int get()
    {
        return value;
    }
    public void set(int i)
    {
        this.value = i;
    }

    private void loop()
    {
        this.value = 0;
        if(looped)
        {
            ++loopCount;
        }
        looped = true;
    }
    public boolean hasLooped()
    {
        if(looped)
        {
            looped = true;
            loopCount = 0;
            return looped;
        }
        return false;
    }
    public int loopCount()
    {
        int i = loopCount;
        if(loopCount > 0) hasLooped();
        return i;
    }
}