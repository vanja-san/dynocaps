package com.biom4st3r.dynocaps.items;

import java.util.List;

import com.biom4st3r.biow0rks.BioLogger;
import com.biom4st3r.dynocaps.ModInit;
import com.biom4st3r.dynocaps.components.DynocapComponent;
import com.biom4st3r.dynocaps.components.IDynocapComponent;
import com.biom4st3r.dynocaps.components.IDynocapComponent.Result;

import nerdhub.cardinal.components.api.component.ComponentProvider;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.item.TooltipContext;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.network.MessageType;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.text.LiteralText;
import net.minecraft.text.Text;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.Formatting;
import net.minecraft.util.Hand;
import net.minecraft.util.TypedActionResult;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Box;
import net.minecraft.util.math.Direction;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;

/**
 * DynocapItem
 */
public class DynocapItem extends Item  {

    public static BioLogger logger = new BioLogger("dynocap:item");

    public DynocapItem() {
        super(new Settings().group(ModInit.DYNO_GROUP).maxCount(1));
    }

    @Override
    public TypedActionResult<ItemStack> use(World world, PlayerEntity user, Hand hand) {
        ItemStack stack = user.getStackInHand(hand);
        IDynocapComponent component = ModInit.dynocapType.get(ComponentProvider.fromItemStack(stack));
        
        if(user.isSneaking())
        {
            if(world.isClient)
            {
                ModInit.openGUI(component.isFilled());
            }
            return TypedActionResult.success(stack);
        }
        if(world.isClient) return TypedActionResult.success(stack);

        int width = component.getWidth(), height = component.getHeight(), depth = component.getDepth();
        if (!component.isFilled()) {
            ItemStack newStack = new ItemStack(ModInit.DynoCap, 1);
            DynocapComponent newComponent = (DynocapComponent) ComponentProvider.fromItemStack(newStack)
                    .getComponent(ModInit.dynocapType);

            newComponent.copyFrom(component);
            Vec3d pos = user.getPosVector();
            pos = new Vec3d(Math.floor(pos.x), Math.floor(pos.y), Math.floor(pos.z));
            Box box = new Box(user.getBlockPos(),
                    new BlockPos(pos.getX() + width, pos.getY() + height, pos.getZ() + depth));
            Direction dir = user.getHorizontalFacing();
            if (dir == Direction.NORTH) {
                box = box.offset(-Math.floor(newComponent.getWidth() / 2f), 0, -(depth + 1));
            } else if (dir == Direction.EAST) {
                box = box.offset(2, 0, -Math.floor(newComponent.getDepth() / 2f));
            } else if (dir == Direction.SOUTH) {
                box = box.offset(-Math.floor(newComponent.getWidth() / 2f), 0, 2);
            } else if (dir == Direction.WEST) {
                box = box.offset(-(width + 1), 0, -Math.floor(newComponent.getDepth() / 2f));
            }
            stack.decrement(1);
            newComponent.getBoxContents(world, box, true);
            return TypedActionResult.success(newStack);

        } else {
            Result result = Result.SUCCESS;
            Direction dir = user.getHorizontalFacing();
            if (dir == Direction.NORTH) {
                result = component.releaseBlocks(world,
                        user.getBlockPos().add(-Math.floor(component.getWidth() / 2f), 0, -(depth + 1)));
            } else if (dir == Direction.EAST) {
                result = component.releaseBlocks(world,
                        user.getBlockPos().add(2, 0, -Math.floor(component.getDepth() / 2f)));
            } else if (dir == Direction.SOUTH) {
                result = component.releaseBlocks(world,
                        user.getBlockPos().add(-Math.floor(component.getWidth() / 2f), 0, 2));
            } else if (dir == Direction.WEST) {
                result = component.releaseBlocks(world,
                        user.getBlockPos().add(-(width + 1), 0, -Math.floor(component.getDepth() / 2f)));

            }
            switch (result) {
                case PREVENT_OVERRIDES:
                    ServerPlayerEntity.class.cast(user).sendChatMessage(
                            new TranslatableText("dialog.dynocaps.PREVENT_OVERRIDES"), MessageType.SYSTEM);
                case PROHIBIITED_BLOCK:
                    ServerPlayerEntity.class.cast(user).sendChatMessage(new TranslatableText("dialog.dynocaps.PROHIBIITED_BLOCK"),
                            MessageType.SYSTEM);
                case SUCCESS:
                    component.clearStorage();
            }
        }
        return TypedActionResult.pass(stack);
    }

    @Environment(EnvType.CLIENT)
    @Override
    public Text getName(ItemStack stack) {
        if (stack != null) {
            IDynocapComponent component;
            if ((component = ModInit.dynocapType.get(stack)) != null) {
                return new LiteralText(component.getName());
            }
        }
        return super.getName(stack);
    }

    @Environment(EnvType.CLIENT)
    @Override
    public void appendTooltip(ItemStack stack, World world, List<Text> tooltip, TooltipContext context) {
        if(ModInit.dynocapType.get(stack).isFilled())
        {
            tooltip.add(new TranslatableText("dialog.dynocaps.filled").formatted(Formatting.GRAY));
        }
    }
    // @Override
    // public int getInvSize(ItemStack invItem) {
    //     return 0;
    // }

    // @Override
    // public ItemStack getStack(ItemStack invItem, int index) {
    //     IDynocapComponent component = ModInit.dynocapType.get(invItem);
    //     BlockStorage storage = component.getBoxContents(null, null, false).get(index);
    //     if(storage.isFluid()) return 
    //     ItemStack is = new ItemStack(storage.state.getBlock().asItem());

    //     if(storage.isBlockEntity())
    //     {
    //         is.setTag(storage.entityTag);
    //     }
    //     return is;
    // }

    // @Override
    // public void setStack(ItemStack invItem, int index, ItemStack stack) {
    //     IDynocapComponent component = ModInit.dynocapType.get(invItem);
    //     List<BlockStorage> storage = component.getBoxContents(null, null, false);
    //     BlockItem block = (BlockItem) invItem.getItem();
    //     BlockStorage bs = new BlockStorage();
    //     bs.state = block.getBlock().getDefaultState();
    //     if(block.getBlock() instanceof BlockEntityProvider)
    //     {
    //         bs.entityTag = ((BlockEntityProvider)block.getBlock()).createBlockEntity(null).toTag(new CompoundTag());
    //     }
    //     bs.relativePos = storage.get(index).relativePos;
    //     storage.set(index, bs);
    // }

    // @Override
    // public boolean canInsert(ItemStack invItem, int index, ItemStack stack) {
    //     if(!(invItem.getItem() instanceof BlockItem)) return false;
    //     if(invItem.getCount() > 1) return false;
    //     IDynocapComponent component = ModInit.dynocapType.get(invItem);
    //     List<BlockStorage> storage = component.getBoxContents(null, null, false);
    //     return storage.get(index).state.isAir();
    // }

    // @Override
    // public boolean canTake(ItemStack invItem, int index) {
    //     return true;
    // }

}