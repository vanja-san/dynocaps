package com.biom4st3r.dynocaps.items;

import java.util.List;

import com.biom4st3r.dynocaps.ModInit;

import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.fabricmc.fabric.api.container.ContainerProviderRegistry;
import net.minecraft.client.item.TooltipContext;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.Inventory;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.text.LiteralText;
import net.minecraft.text.Text;
import net.minecraft.util.Formatting;
import net.minecraft.util.Hand;
import net.minecraft.util.TypedActionResult;
import net.minecraft.world.World;

/**
 * DynoCapCaseItem
 */
public class DynoCapCaseItem extends Item {// implements ItemInventory {

    public DynoCapCaseItem() {
        super(new Settings().maxCount(1).group(ModInit.DYNO_GROUP));
    }

    @Override
    public TypedActionResult<ItemStack> use(World world, PlayerEntity user, Hand hand) {
        ItemStack stack = user.getStackInHand(hand);
        if (world.isClient)
            return TypedActionResult.pass(stack);
        ContainerProviderRegistry.INSTANCE.openContainer(ModInit.containerid, user, (pbb) -> {
            pbb.writeByte(user.inventory.selectedSlot);
        });
        return TypedActionResult.success(stack);
    }

    // @Override
    // public int getInvSize(ItemStack invItem) {
    //     return ModInit.inventoryType.get(invItem).getInvSize();
    // }

    // @Override
    // public ItemStack getStack(ItemStack invItem, int index) {
    //     return ModInit.inventoryType.get(invItem).getInvStack(index);
    // }

    // @Override
    // public void setStack(ItemStack invItem, int index, ItemStack stack) {
    //     ModInit.inventoryType.get(invItem).setInvStack(index, stack);
    // }

    // @Override
    // public boolean canTake(ItemStack invItem, int index) {
    //     return true;
    // }

    // @Override
    // public boolean canInsert(ItemStack invItem, int index, ItemStack stack) {
    //     return invItem.getItem() == ModInit.DynoCap;
    // }

    @Environment(EnvType.CLIENT)
    @Override
    public void appendTooltip(ItemStack stack, World world, List<Text> tooltip, TooltipContext context) {
        Inventory inv = ModInit.inventoryType.get(stack);
        for(int i = 0; i < inv.getInvSize(); i++)
        {
            if(inv.getInvStack(i).isEmpty())
            {
                tooltip.add(new LiteralText("dialog.dynocaps.empty").formatted(Formatting.GRAY));
            }
            else
            {
                tooltip.add(new LiteralText(ModInit.dynocapType.get(inv.getInvStack(i)).getName()).formatted(Formatting.GRAY));
            }
        }
    }
    
}