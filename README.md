
[![](https://hitcounter.pythonanywhere.com/count/tag.svg?url=https%3A%2F%2Fgitlab.com%2Fbiom4st3r%2Fdynocaps)]()
[![](https://jitpack.io/v/com.gitlab.biom4st3r/dynocaps.svg)](https://jitpack.io/#com.gitlab.biom4st3r/dynocaps)
# DynoCaps
![Imgur](https://i.imgur.com/ZpKJjlI.png)

## About
Dynocaps let you explore the world from the comfort of your own house. It's always a pain in the $$@ when you decide you want to up and find a new place to live, but now you can just shove you house and valuable in a Dynocap and off you go.

## Notables
* Anviless renaming
* When holding your Dynocap the affected area is outlined
* 16,777,216 color support to help you sort your caps
* Convenient carrying case that hold up to 4 caps and displays which ones are inside
* If you can't remember why you named your cap ```Weird thing in woods 7```, no worries! A preview is avalible in your filled caps GUI

## Issues
If you find an issue please report it at my [Gitlab](https://gitlab.com/biom4st3r/dynocaps/issues).
If you don't have, or want to create, a gitlab to can send an email to ```incoming+biom4st3r-dynocaps-16652264-issue-@incoming.gitlab.com``` and an issue will be automaticlly created.

Feel free to request new features also.

```If your issue is related to a crash: please provide a crash log link hosted on pastebin/hastebin/etc```

[Pastebin](https://pastebin.com)

[Hastebin](https://hastebin.com)



## FAQ
* Will you forge? No

## License
[GNU General Public License 3](https://gitlab.com/biom4st3r/dynocaps/-/blob/master/LICENSE)



